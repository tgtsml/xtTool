#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QWidget>
#include <QComboBox>

class QTextEdit;
class QTcpSocket;
class QPushButton;
class QTimer;
class QSpinBox;

class TCPClient : public QWidget
{
    Q_OBJECT
public:
    explicit TCPClient(QWidget *parent = nullptr);
    ~TCPClient();

signals:
    void signalUpdateTitle(QString text);

private:
    void initWidget();
    QStringList getAvailableComs();
    void updateControlState();
    void addLog(QString log);

    void slot_connected();
    void slot_disconnected();

    void slot_netOpen();
    void slot_readyRead();
    void slot_netSend();

    void slot_hexRecieve(int state);
    void slot_showTime(int state);
    void slot_wordWrap(int state);
    void slot_hexSend(int state);
    void slot_autoSend(int state);

    struct{
        bool showTime;
        bool showHex;
        bool sendHex;
    }m_netSettings;
    QTextEdit *m_textSend, *m_textRecv;
    QTcpSocket *m_tcpSocket;
    QPushButton *m_btnOpen, *m_btnSend;
    QTimer *m_autoSendTimer;
    QSpinBox *m_spinBoxSendMs;

    QLineEdit *m_ctrlTargetIP, *m_ctrTargetPort;
};

#endif // TCPCLIENT_H
