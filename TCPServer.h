#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QWidget>
#include <QMap>

class QPushButton;
class QLineEdit;
class QTextEdit;
class QComboBox;

class QTcpServer;
class QTcpSocket;
class QSpinBox;

class TCPServer : public QWidget
{
    Q_OBJECT
public:
    explicit TCPServer(QWidget *parent = nullptr);
    ~TCPServer();

private:
    void initWidget();
    void addLog(QString text);
    QString getCurrentIPV4();

    void slotBtnClickedListen();
    void slotBtnClickedSend();
    void slotHexRecieve(int state);
    void slotShowTime(int state);
    void slotWordWrap(int state);
    void slotHexSend(int state);
    void slotAutoSend(int state);

    void newConnection();
    void slotReadyRead();
    void slotDisconnected();

    void updateControlState();

signals:
    void signalUpdateTitle(QString text);

private:
    QPushButton *m_btnListen, *m_btnSend;
    QComboBox *m_ctrServerIP;
    QLineEdit *m_ctrServerPort;
    QTextEdit *m_textSend, *m_textRecv;
    QComboBox *m_comboxClients;
    QSpinBox *m_spinBoxSendMs;

    QTcpServer *m_tcpServer;
    QMap<QString, QTcpSocket*> mapTcpClients;

    struct{
        bool showTime;
        bool showHex;
        bool sendHex;
    }m_netSettings;
    QTimer *m_autoSendTimer;
};

#endif // TCPSERVER_H
