#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QWidget>
#include <QComboBox>

class QTextEdit;
class QSerialPort;
class QPushButton;
class QTimer;
class QSpinBox;

class SerialPort : public QWidget
{
    Q_OBJECT
public:
    explicit SerialPort(QWidget *parent = nullptr);
    ~SerialPort();

signals:
    void signalUpdateTitle(QString text);

private:
    void initWidget();
    QStringList getAvailableComs();
    void updateControlState(bool connected);
    void addLog(QString log);
    void updateComNamesComboxView(QStringList tmpComs);

    void slot_comOpen();
    void slot_comRecieve();
    void slot_comSend();
    void slot_hexRecieve(int state);
    void slot_showTime(int state);
    void slot_wordWrap(int state);
    void slot_hexSend(int state);
    void slot_autoSend(int state);
    void slot_updateComs();

    enum ComSetIndex{
        ComName,
        BaudRate,
        DataBits,
        Parity,
        StopBits,
        FlowControl,
    };
    struct{
        bool showTime;
        bool showHex;
        bool sendHex;
    }m_comSettings;
    QMap<ComSetIndex, QComboBox*> m_comSetControls;
    QTextEdit *m_textSend, *m_textRecv;
    QSerialPort *m_serialPort;
    QPushButton *m_btnOpen, *m_btnSend;
    QTimer *m_autoSendTimer;
    QSpinBox *m_spinBoxSendMs;
    QComboBox *m_comboxComs;
    QMap<QString, QString> m_comsMap;
};

#endif // SERIALPORT_H
